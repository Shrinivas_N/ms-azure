# SenseHAT
The Sense HAT is an add-on electronic board (or HAT) for the Raspberry Pi computer. It has sensors, a mini joystick and a grid of multi-colour pixels.
The Sense HAT has an 8×8 RGB LED matrix, a five-button joystick and includes the following sensors:
    Accelerometer (movement)
    Barometer (pressure)
    Gyroscope (rotation)
    Hygrometer (humidity)
    Joystick (basic input)
    Magnetometer (direction)
    Thermometer (temperature)

## Install

```bash
sudo apt-get update
sudo apt-get install sense-hat
```

## Usage

Hello world example:
```python
from sense_hat import SenseHat
sense = SenseHat()
sense.show_message("Hello world!")
```
Run the samples in the Exercise 2 directory
