import ConfigParser
import os
import time
from sense_hat import SenseHat

sense = SenseHat()
r = (255,0,0)
m = (127,127,0)
g = (0,255,0)
l = (0,0,255)
b = (0,0,0)

notFound = [
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b
]

veryFar = [
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, m, m, b, b, b,
        b, b, b, m, m, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b
]

far = [
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, g, g, g, g, b, b,
        b, b, g, g, g, g, b, b,
        b, b, g, g, g, g, b, b,
        b, b, g, g, g, g, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b
]

close = [
        b, b, b, b, b, b, b, b,
        b, r, r, r, r, r, r, b,
        b, r, r, r, r, r, r, b,
        b, r, r, r, r, r, r, b,
        b, r, r, r, r, r, r, b,
        b, r, r, r, r, r, r, b,
        b, r, r, r, r, r, r, b,
        b, b, b, b, b, b, b, b
]

veryClose = [
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l,
        l, l, l, l, l, l, l, l
]

def readConfig():
   global ble_id
   global scan_interval
   global p_threshold 
   global a_threshold
   

   configParser = ConfigParser.RawConfigParser()   
   configFilePath = r'config.ini'
   configParser.read(configFilePath)
   ble_id = configParser.get('app-config', 'ble_id')
   
   print(ble_id)
   
if __name__=='__main__':
   sense.clear((0,0,0))
   readConfig()
   status = "unknown"
   counter=1
   myOutPut = 1
   while True:
      tempOP = "1"
      command = "sudo btmgmt --timeout 1 find | grep "+ ble_id + " | awk '{print $8}' | head -n 1"
      tempOP = os.popen(command).read()
      if ( tempOP != '' ):
        counter=1
        myOutPut = int(tempOP,10)
      else:
        counter += 1
      print(myOutPut)

      if ( counter == 20 ):
          myOutPut = 1

      if ( (myOutPut < 0) and (myOutPut > -40) ) :
          sense.set_pixels(veryClose)
      elif ( (myOutPut < -39) and (myOutPut > -70) ):
          sense.set_pixels(close)
      elif ( (myOutPut < -69) and (myOutPut > -100) ):
          sense.set_pixels(far)
      elif ( (myOutPut < -99) ) :
          sense.set_pixels(veryFar)
      else:
          sense.set_pixels(notFound)
