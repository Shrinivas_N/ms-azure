# Microsoft Azure IoTHub Python SDK
Create and manage an instance of IoT Hub and IoT devices in your Azure subscription using Python SDK

## Setup and Build Azure IoT Hub SDK for Python on Raspberry Pi/Linux:

Note: Setup will default to python 2.7. Check Python version 
```bash
python --version
```
Open Raspberry Pi terminal and enter the following commands
1. Update packages and install dependencies
 ```bash
sudo apt-get update
sudo apt-get install -y git cmake build-essential curl libcurl4-openssl-dev libssl-dev uuid-dev
```
2. Varify that CMake version is atleast version 2.8.12
```bash
cmake --version
```
4. Varify that gcc is atleast version 4.4.7
```bash
gcc --version
```
5. Clone the Azure IoTHub SDK Python
```bash 
git clone --recursive --branch v1-deprecated https://github.com/Azure/azure-iot-sdk-python.git
```
6. Navigate to build_all/linux in your local copy of repository
```bash
cd azure-iot-sdk-python/build_all/linux
```
7. Run below script to install prerequisite packages and dependent libraries
```bash
sudo ./setup.sh
```
8. Run below script to build
```bash
sudo ./build.sh
```
After successful build, the __iothub_client.so__ python extension module is copied to the __devices/samples__ and __services/samples.__



