from sense_hat import SenseHat
import datetime

sense= SenseHat()

class Sensehat:

    def __init__(self):
        pass

    def read_temperature(self):
        temp = sense.get_temperature()
	return round(temp, 1)

    def read_humidity(self):
        humid = sense.get_humidity()
	return round(humid, 1)

    def read_pressure(self):
        press = sense.get_pressure()
	return round(press, 1)
    
